module.exports = {
  collectCoverageFrom: ["src/**/*.js"],
  testPathIgnorePatterns: ["/node_modules/"],
  testEnvironment: "node",
  verbose: true
};
