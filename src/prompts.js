const inquirer = require("inquirer");

/**
 * Ask user to type an article title
 *
 * @function askArticleTitle
 * @returns {String} article title
 */
const askArticleTitle = async () =>
  inquirer
    .prompt([
      {
        type: "input",
        name: "qry",
        default: "Sevilla",
        message: "Wiki Reader > What article?"
      }
    ])
    .then(({ qry }) => qry);

/**
 * Ask user if want to continue reading
 *
 * @returns {Boolean}
 */
const continueReading = async () =>
  inquirer
    .prompt([
      {
        type: "confirm",
        name: "reading",
        message: "Wiki Reader > Continue reading?"
      }
    ])
    .then(({ reading }) => reading);

/**
 * Ask user for the task to perform
 *
 * @param {Function} cb that will be called to process user answer
 * @returns {Boolean} continue or quit
 */
const wikiReader = cb =>
  inquirer
    .prompt([
      {
        type: "list",
        choices: ["read", "random", "quit"],
        name: "cmd",
        message: "Wiki Reader >"
      }
    ])
    .then(cb);

module.exports = {
  askArticleTitle,
  continueReading,
  wikiReader
};
