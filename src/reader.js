const { continueReading } = require("./prompts");

/**
 * Read an article by paragraphs
 *
 * @param {Array} paragraphs
 */
async function reader(wikiPage) {
  const paragraphs = wikiPage.paragraphs();
  let index = 0;
  let readMore = true;

  while (index < paragraphs.length && readMore) {
    process.stdout.write(`\n${paragraphs[index].text()}\n\n`);
    index++;
    readMore = await continueReading();
  }

  if (index === paragraphs.length) {
    process.stdout.write("\nEnd of the article.\n");
  }
}

module.exports = {
  reader
};
