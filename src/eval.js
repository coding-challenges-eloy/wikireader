async function evalCmd({ cmd }) {
  const command = cmd.trim().toLowerCase();

  let evalCommand = () => {};

  try {
    evalCommand = require(`./commands/${command.toLowerCase()}`);
    return evalCommand();
  } catch (error) {
    const errMess = `Cannot find module './commands/${command.toLowerCase()}'`;

    if (error.message === errMess) {
      console.error(`Command: ${command} doesn't exits.`);
    } else {
      console.error(error);
    }
  }
}

module.exports = { evalCmd };
