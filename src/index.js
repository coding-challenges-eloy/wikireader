const { wikiReader } = require("./prompts");
const { evalCmd } = require("./eval");
const { initWikipedia } = require("./wikipedia");

const lang = process.argv[2] || "en"; // default to english

if (!initWikipedia(lang)) {
  process.stdout.write(
    `\nThere is no wikipedia available in your language ${lang}\n`
  );
  process.exit();
}

(async function() {
  let keepREading = true;

  while (keepREading) {
    keepREading = await wikiReader(evalCmd);
  }

  process.stdout.write("\nThanks for using Wiki Reader!\n");
})();
