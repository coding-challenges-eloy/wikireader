const wikipedia = require("wtf_wikipedia");
const wikipedias = require("./languages.json");

let language = null;

/**
 * Fetch a random wikipedia page and return a list of paragraphs
 *
 * @function getRandomWikiPage
 * @returns {Array}
 */
const getRandomWikiPage = async () => wikipedia.random(language);

/**
 * Fetch wikipedia page and return a list of paragraphs
 *
 * @function getWikiPage
 * @param {String} title
 * @returns {Array}
 */
const getWikiPage = async title => wikipedia.fetch(title, language);

/**
 * Check if a wikipedia exists for the given language tag
 * and persist language in module
 *
 * @param {String} lang
 * @returns {Boolean}
 */
function initWikipedia(lang) {
  const validLang = !!wikipedias.filter(({ Wiki }) => Wiki === lang).length;
  if (validLang) {
    language = lang;
    return true;
  } else {
    return false;
  }
}

module.exports = {
  getRandomWikiPage,
  getWikiPage,
  initWikipedia
};
