const wikipedias = require("./languages.json");

const minw = wikipedias.map(({ Language, Wiki, ...other }) => ({
  Language,
  Wiki,
  localized: other["Language (local)"]
}));

process.stdout.write(JSON.stringify(minw));
