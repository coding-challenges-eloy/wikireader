const { askArticleTitle } = require("../prompts");
const { reader } = require("../reader");
const { getWikiPage } = require("../wikipedia");

async function read() {
  // ask for article title
  const title = await askArticleTitle();

  try {
    // get wikipedia article
    const wikiPage = await getWikiPage(title);
    await reader(wikiPage);
  } catch (error) {
    console.error(error);

    process.stdout.write(`\nCannot find any wikipedia article '${title}'.\n`);
  }

  return true;
}

module.exports = read;
