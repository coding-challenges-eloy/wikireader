const { reader } = require("../reader");
const { getRandomWikiPage } = require("../wikipedia");

async function random() {
  // get a random wikipedia article
  const wikiPage = await getRandomWikiPage();
  // print page title
  process.stdout.write(`\nWikipedia page: '${wikiPage.title()}'\n`);

  await reader(wikiPage);

  return true;
}

module.exports = random;
