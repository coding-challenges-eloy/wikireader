const random = require("./random");

jest.mock("../reader", () => ({
  reader: jest.fn()
}));
jest.mock("../wikipedia", () => ({
  getRandomWikiPage: jest.fn(() => ({ title: () => "Title" }))
}));

test("random command runs succesfully", async () => {
  expect(await random()).toBe(true);
});
