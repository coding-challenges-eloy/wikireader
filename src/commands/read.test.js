const read = require("./read");

jest.mock("../prompts", () => ({
  askArticleTitle: jest.fn()
}));
jest.mock("../reader", () => ({
  reader: jest.fn()
}));
jest.mock("../wikipedia", () => ({
  getWikiPage: jest.fn()
}));

test("read command runs succesfully", async () => {
  expect(await read()).toBe(true);
});
