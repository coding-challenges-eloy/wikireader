async function keypress() {
  // process.stdin.setRawMode(true);
  process.stdin.setEncoding("utf8");

  process.stdout.write("Press any key to continue reading or ESC to finish ");

  return new Promise(resolve =>
    process.stdin.once("data", key => {
      // process.stdin.setRawMode(false);
      process.stdout.write("\n");

      if (key === "\u001B") {
        resolve(false);
      } else {
        resolve(true);
      }
    })
  );
}

async function reading(article) {
  // if (index >= article.length) {
  //   process.stdout.write("End of the article\n\n");
  //   return;
  // }
  let index = 0;
  let readMore = true;

  do {
    process.stdout.write(`${article[index]}\n\n`);
    index++;
    readMore = await keypress();
  } while (index < article.length && readMore);

  // if (readMore) {
  //   await reading(article, index + 1);
  // } else {
  //   return;
  // }
}

async function read() {
  await reading([
    "First paragraph of wikipedia article",
    "Second paragraph of wikipedia article",
    "Third paragraph of wikipedia article"
  ]);
}

module.exports = read;
