const repl = require("repl");

async function myEval(cmd, context, filename, callback) {
  const [command, argument] = cmd
    .split("\n")
    .shift()
    .split(" ");

  let evalCommand = () => {};
  let result = "";

  try {
    //
    evalCommand = require(`./commands/${command.toLowerCase()}`);
    result = await evalCommand(argument);
  } catch (error) {
    const errMess = `Cannot find module './commands/${command.toLowerCase()}'`;

    if (error.message === errMess) {
      console.error(`Command: ${command} doesn't exits.`);
    } else {
      console.error(error);
    }
  }

  // switch (command) {
  //   case "READ":
  //     console.log(`Article: ${article}.`);
  //     break;

  //   default:
  //     console.error(`Command: ${command} doesn't exits.`);
  //     break;
  // }
  process.stdout.clearLine();

  callback();
}

repl.start({ prompt: "> ", eval: myEval });
